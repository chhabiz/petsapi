﻿using System.Collections.Generic;
using System.Web.Http.Results;
using CatWebApi.API.Controllers;
using CatWebApi.API.Interfaces;
using CatWebApi.API.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CatWebApi.API.Tests
{
    [TestClass]
    public class OwnerControllerTest
    {

        [TestMethod]
        public void TestGetPetsByOwnersGenderAndType()
        {
            ////Arrange
            var mockRepository = new Mock<IRepository<PetsByOwnerGenderDTO>>();
            var mockOwnerRepository = new Mock<IRepository<OwnerDTO>>();

            var owner = new PetsByOwnerGenderDTO()
            {
                Gender = "Male",
                Pets = new List<PetDTO> { new PetDTO { Name = "Angel", Type = "Cat" }, new PetDTO { Name = "Molly", Type = "Cat" } }
            };
            var ownersByGender = new List<PetsByOwnerGenderDTO> { owner };

            mockRepository
                .Setup(x => x.GetByGenderAndType("male", "cats"))
                .Returns(ownersByGender);

            OwnerController controller = new OwnerController(mockOwnerRepository.Object, mockRepository.Object);

            //Act            
            var response = controller.GetPetsByOwnersGender("male", "cats");
            var contentResult = ((OkNegotiatedContentResult<IEnumerable<PetsByOwnerGenderDTO>>)response).Content;

            //Assert
            Assert.AreEqual(ownersByGender, contentResult);
        }

        [TestMethod]
        public void TestGetOwnersByGender()
        {
            ////Arrange
            var mockRepository = new Mock<IRepository<PetsByOwnerGenderDTO>>();
            var mockOwnerRepository = new Mock<IRepository<OwnerDTO>>();

            var owner = new OwnerDTO()
            {
                Name = "John Smith",
                Gender = "Male",
                Age = "23",
                Pets = new List<PetDTO> { new PetDTO { Name = "Angel", Type = "Cat" }, new PetDTO { Name = "Sam", Type = "Dog" } }
            };
            var ownersByGender = new List<OwnerDTO> { owner };

            mockOwnerRepository
                .Setup(x => x.GetByGender("male"))
                .Returns(ownersByGender);

            OwnerController controller = new OwnerController(mockOwnerRepository.Object, mockRepository.Object);

            //Act            
            var response = controller.GetOwnersByGender("male");
            var contentResult = ((OkNegotiatedContentResult<IEnumerable<OwnerDTO>>)response).Content;

            //Assert
            Assert.AreEqual(ownersByGender, contentResult);
        }

        [TestMethod]
        public void TestGetAllOwners()
        {
            ////Arrange
            var mockRepository = new Mock<IRepository<PetsByOwnerGenderDTO>>();
            var mockOwnerRepository = new Mock<IRepository<OwnerDTO>>();

            var owner = new OwnerDTO()
            {
                Name = "John Smith",
                Gender = "Male",
                Age = "23",
                Pets = new List<PetDTO> { new PetDTO { Name = "Angel", Type = "Cat" }, new PetDTO { Name = "Sam", Type = "Dog" } }
            };
            var ownersByGender = new List<OwnerDTO> { owner };

            mockOwnerRepository
                .Setup(x => x.GetAll())
                .Returns(ownersByGender);

            OwnerController controller = new OwnerController(mockOwnerRepository.Object, mockRepository.Object);

            //Act            
            var response = controller.GetOwners();
            var contentResult = ((OkNegotiatedContentResult<IEnumerable<OwnerDTO>>)response).Content;

            //Assert
            Assert.AreEqual(ownersByGender, contentResult);
        }

        [TestMethod]
        public void TestGetAllPetsByOwnersGender()
        {
            ////Arrange
            var mockRepository = new Mock<IRepository<PetsByOwnerGenderDTO>>();
            var mockOwnerRepository = new Mock<IRepository<OwnerDTO>>();

            var owner = new PetsByOwnerGenderDTO()
            {
                Gender = "Male",
                Pets = new List<PetDTO> { new PetDTO { Name = "Angel", Type = "Cat" }, new PetDTO { Name = "Molly", Type = "Cat" } }
            };
            var ownersByGender = new List<PetsByOwnerGenderDTO> { owner };

            mockRepository
                .Setup(x => x.GetByGender("male"))
                .Returns(ownersByGender);

            OwnerController controller = new OwnerController(mockOwnerRepository.Object, mockRepository.Object);

            //Act            
            var response = controller.GetAllPetsByOwnersGender("male");
            var contentResult = ((OkNegotiatedContentResult<IEnumerable<PetsByOwnerGenderDTO>>)response).Content;

            //Assert
            Assert.AreEqual(ownersByGender, contentResult);
        }
    }
}
