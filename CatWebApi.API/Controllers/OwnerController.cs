﻿using System.Linq;
using System.Web.Http;
using CatWebApi.API.Helpers;
using CatWebApi.API.Interfaces;
using CatWebApi.API.Models;

namespace CatWebApi.API.Controllers
{
    [RoutePrefix("api/owners")]
    public class OwnerController : ApiController
    {
        //dependency injected through constructor injection
        private readonly IRepository<OwnerDTO> _repo;
        private readonly IRepository<PetsByOwnerGenderDTO> _oRepo;
        public OwnerController(IRepository<OwnerDTO> repo, IRepository<PetsByOwnerGenderDTO> oRepo)
        {
            this._repo = repo;
            this._oRepo = oRepo;
        }

        [Route("")]
        public IHttpActionResult GetOwners()
        {
            var owners = _repo.GetAll();
            return Ok(owners);
        }

        [Route("{gender}")]
        public IHttpActionResult GetOwnersByGender(string gender)
        {
            if (!Helper.IsValidGender(gender))
            {
                return BadRequest("invalid gender");
            }

            var owners = _repo.GetByGender(gender.ToLower());
            if (owners.Count() == 0)
            {
                return NotFound();
            }
            return Ok(owners);
        }

        [Route("{gender}/pets/{type}")]
        public IHttpActionResult GetPetsByOwnersGender(string gender, string type)
        {
            if (!Helper.IsValidGender(gender))
            {
                return BadRequest("invalid gender");
            }
            var pets = _oRepo.GetByGenderAndType(gender, type);

            if (pets == null)
            {
                return NotFound();
            }
            if (pets.Count() == 0)
            {
                return NotFound();
            }
            return Ok(pets);
        }

        [Route("{gender}/pets/")]
        public IHttpActionResult GetAllPetsByOwnersGender(string gender)
        {
            if (!Helper.IsValidGender(gender))
            {
                return BadRequest("invalid gender");
            }
            var pets = _oRepo.GetByGender(gender);

            if (pets == null)
            {
                return NotFound();
            }
            if (pets.Count() == 0)
            {
                return NotFound();
            }
            return Ok(pets);
        }
    }
}
