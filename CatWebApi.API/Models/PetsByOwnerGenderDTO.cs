﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CatWebApi.API.Models
{
    public class PetsByOwnerGenderDTO
    {
        public string Gender { get; set; }
        public IEnumerable<PetDTO> Pets { get; set; }
    }
}