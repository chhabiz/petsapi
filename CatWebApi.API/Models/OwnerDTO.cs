﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CatWebApi.API.Models
{
    public class OwnerDTO
    {
        public string Name { get; set; }
        public string Gender { get; set; }
        public string Age { get; set; }
        public IEnumerable<PetDTO> Pets { get; set; }
    }
}