﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CatWebApi.API.Models
{
    public class PetDTO
    {
        public string Name { get; set; }
        public string Type { get; set; }
    }
}