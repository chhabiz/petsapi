﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CatWebApi.API.Models;

namespace CatWebApi.API.Helpers
{
    public static class Helper
    {
        public static bool IsValidGender(string gender)
        {
            if (gender.ToLower().Equals("male") || gender.ToLower().Equals("female"))
            {
                return true;
            }
            return false;
        }
    }
}