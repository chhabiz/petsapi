﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CatWebApi.API.Models;

namespace CatWebApi.API.Interfaces
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> GetAll();
        T GetByName(string name);
        IEnumerable<T> GetByGender(string gender);
        IEnumerable<T> GetByType(string type);
        IEnumerable<T> GetByGenderAndType(string gender, string type);
    }
}
