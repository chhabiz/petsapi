﻿console.clear();

class Owner extends React.Component {
    constructor() {
        super();
        this.state = {
            data: '',
            loading: true
        };
    }

    componentDidMount() {
        const url = this.props.apiUrl;
        axios.get(url)
            .then(response => {
                this.setState({
                    data: response.data,
                    loading: false
                });
            })
            .catch(error => {
                console.log(error);
            });
    }

    render() {
        let content;

        if (this.state.loading) {
            content = <div>Loading...</div>;
        } else {
            content = this.state.data.map((owner, index) => {
                return (
                    <div key={index} className="panel panel-primary">
                        <h2 className="panel-heading">{owner.Gender}</h2>
                        <ul className="panel-body list-unstyled list-group">
                            {
                                owner.Pets.map((pet, i) => {
                                    return (
                                        <li key={i} className="list-group-item"> &gt; {pet.Name}</li>
                                    )
                                })
                            }
                        </ul>
                    </div>
                )
            });
        }

        return (
            <div>
                {content}
            </div>
        )
    }
}

class App extends React.Component {
    render() {
        return (
            <div>
                <Owner apiUrl={'http://localhost:51486/api/owners/male/pets/cat'} />
                <Owner apiUrl={'http://localhost:51486/api/owners/female/pets/cat'} />
            </div>
        );
    }
}

ReactDOM.render(
    <App />,
    document.getElementById('app')
);