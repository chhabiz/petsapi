﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CatWebApi.API.Interfaces;
using CatWebApi.API.Models;

namespace CatWebApi.API.Repository
{
    public class OwnerByGenderRepository : IRepository<PetsByOwnerGenderDTO>
    {
        private readonly OwnerContext _db = new OwnerContext();

        public IEnumerable<PetsByOwnerGenderDTO> GetAll()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<PetsByOwnerGenderDTO> GetByGender(string gender)
        {
            //remove all owners without pets to avoid exceptions caused by nulls
            using (_db)
            {
                var petsByOwner = _db.Owners.Where(x => x != null).Where(x => x.Pets != null).GroupBy(u => u.Gender)
                          .Select(grp => new PetsByOwnerGenderDTO { Gender = grp.Key, Pets = grp.SelectMany(x => x.Pets).OrderBy(x => x.Name).ToList() })
                          .Where(x => x.Gender.ToLower().Equals(gender.ToLower())).ToList();
                if (petsByOwner.SelectMany(x => x.Pets).ToList().Count == 0)
                {
                    return null;
                }
                return petsByOwner;
            }
        }

        public IEnumerable<PetsByOwnerGenderDTO> GetByGenderAndType(string gender, string type)
        {
            //remove all owners without pets to avoid exceptions caused by nulls
            using (_db)
            {
                var petsByOwner =  _db.Owners.Where(x => x != null).Where(x => x.Pets != null).GroupBy(u => u.Gender)
                          .Select(grp => new PetsByOwnerGenderDTO { Gender = grp.Key, Pets = grp.SelectMany(x => x.Pets).Where(x => x.Type.ToLower().Equals(type.ToLower())).OrderBy(x=>x.Name).ToList() })
                          .Where(x=>x.Gender.ToLower().Equals(gender.ToLower())).ToList();
                if(petsByOwner.SelectMany(x=>x.Pets).ToList().Count == 0)
                {
                    return null;
                }
                return petsByOwner;
            }
        }

        public PetsByOwnerGenderDTO GetByName(string name)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<PetsByOwnerGenderDTO> GetByType(string type)
        {
            throw new NotImplementedException();
        }
    }
}