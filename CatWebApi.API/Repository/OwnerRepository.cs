﻿using System;
using System.Collections.Generic;
using System.Linq;
using CatWebApi.API.Interfaces;
using CatWebApi.API.Models;

namespace CatWebApi.API.Repository
{
    public class OwnerRepository : IRepository<OwnerDTO>
    {
        private readonly OwnerContext _db = new OwnerContext();

        //gets all owners and pets list
        public IEnumerable<OwnerDTO> GetAll()
        {
            using (_db)
            {
                return _db.Owners;
            }
        }

        //gets owner details and pet list by owner name
        public OwnerDTO GetByName(string name)
        {
            using (_db)
            {
                return _db.Owners.Where(x => x.Name.ToLower().Equals(name.ToLower())).FirstOrDefault();
            }
        }

        //Gets owner list by owner gender
        public IEnumerable<OwnerDTO> GetByGender(string gender)
        {
            using (_db)
            {
                return _db.Owners.Where(x => x.Gender.ToLower().Equals(gender.ToLower())).ToList();
            }
        }
        public IEnumerable<OwnerDTO> GetByType(string type)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<OwnerDTO> GetByGenderAndType(string gender, string type)
        {
            throw new NotImplementedException();
        }
    }
}