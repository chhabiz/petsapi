﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using CatWebApi.API.Models;
using Newtonsoft.Json;

namespace CatWebApi.API.Repository
{
    public class OwnerContext : IDisposable
    {
        private static string baseUrl = System.Configuration.ConfigurationManager.AppSettings["JsonBaseUrl"];
        private static string jsonLink = System.Configuration.ConfigurationManager.AppSettings["JsonFileName"];
        public IEnumerable<OwnerDTO> Owners { get; set; }
        public  OwnerContext()
        {
            using (var client = new System.Net.Http.HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetAsync(jsonLink).Result;
                using (HttpContent content = response.Content)
                {
                    Task<string> result = content.ReadAsStringAsync();
                    this.Owners = JsonConvert.DeserializeObject<IEnumerable<OwnerDTO>>(result.Result);
                }
            }
        }

        public void Dispose()
        {
        }
    }
}