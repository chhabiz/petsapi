﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CatWebApi.API.Interfaces;
using CatWebApi.API.Models;

namespace CatWebApi.API.Repository
{
    public class PetRepository : IRepository<PetDTO>
    {
        //implements all the methods of IReopsitory
        private readonly OwnerContext _db = new OwnerContext();

        public IEnumerable<PetDTO> GetAll()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<PetDTO> GetByGender(string gender)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<PetDTO> GetByGenderAndType(string gender, string type)
        {
            throw new NotImplementedException();
        }

        public PetDTO GetByName(string name)
        {
            using (_db)
            {
                return _db.Owners.SelectMany(x=>x.Pets).Where(x=>x.Name.ToLower() == name.ToLower()).FirstOrDefault();
            }
        }

        public IEnumerable<PetDTO> GetByType(string type)
        {
            using (_db)
            {
                return _db.Owners.SelectMany(x => x.Pets).Where(x => x.Type.ToLower() == type.ToLower()).OrderBy(x => x.Name).ToList();
            }
        }
    }
}